import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
// Problem that this code is solving can be found here->
// https://www.hackerrank.com/challenges/queens-attack-2/problem
public class Test {

	static int leftBlock = 0;
	static int rightBlock = 0;
	static int upBlock = 0;
	static int downBlock = 0;
	static int leftUpBlock = 0;
	static int leftDownBlock = 0;
	static int rightUpBlock = 0;
	static int rightDownBlock = 0;
	static int queenRow = 0;
	static int queenColumn = 0;
	static int boardSize = 0;
	static int currentRow, currentColumn;

	

	static void blockingIncreasingAxis() {
		// is it on the left of the queen ?
		if (currentColumn < queenColumn) {
			leftDownBlock = leftDownBlock < Math.min(currentColumn, currentRow) ? Math.min(currentColumn, currentRow) : leftDownBlock;
		}
		// no, it's on the right of the queen
		else {
			rightUpBlock = rightUpBlock < (boardSize + 1 - Math.max(currentColumn, currentRow) )? (boardSize + 1 - Math.max(currentColumn, currentRow)): rightUpBlock;
		}

	}

	static void blockingDecreasingAxis() {
		// is it on the left of the queen ?
		if (currentColumn < queenColumn) {
			leftUpBlock = leftUpBlock < Math.min(currentColumn, boardSize+1-currentRow) ? Math.min(currentColumn, boardSize+1-currentRow) : leftUpBlock;
		}
		// no, it's on the right of the queen
		else {
			rightDownBlock = rightDownBlock < Math.min(currentRow, boardSize+1-currentColumn) ?Math.min(currentRow, boardSize+1-currentColumn)
					: rightDownBlock;
		}

	}

	static boolean isBlockingIncreasingAxis() {
		return queenRow - queenColumn == currentRow - currentColumn ? true : false;
	}

	static boolean isBlockingDecreasingAxis() {
		return queenRow - (boardSize + 1 - queenColumn) == currentRow - (boardSize + 1 - currentColumn) ? true : false;

	}

	static void blockingColumn() {
		// is it above the queen ?
		if (currentRow > queenRow) {
			upBlock = upBlock <( boardSize + 1 - currentRow) ? (boardSize + 1 - currentRow) : upBlock;
		}
		// no, it's below the queen
		else {
			downBlock = downBlock < currentRow ? currentRow : downBlock;
		}
	}

	static void blockingRow() {
		// is it on the left of the queen ?
		if (queenColumn > currentColumn) {
			leftBlock = leftBlock < currentColumn ? currentColumn : leftBlock;
		}
		// no, it's on the right of the queen
		else {
			rightBlock = rightBlock < (boardSize + 1 - currentColumn) ? (boardSize + 1 - currentColumn) : rightBlock;
		}
	}

	static void blockingCross() {
		// is it blocking row ?
		if (currentRow == queenRow) {
			blockingRow();
		}
		// no, it's blocking column
		else {
			blockingColumn();

		}
	}

	
	
	
	
	
	static boolean isBlockingCross() {
		return currentRow == queenRow ? true : currentColumn == queenColumn ? true : false;

	}

	static void checkCurrentObstacle() {
		// is it on up&down cross ?
		if (isBlockingCross()) {

			blockingCross();
		}
		// maybe on diagonal increasing from left to right ?
		else if (isBlockingIncreasingAxis()) {
			blockingIncreasingAxis();
		}
		// maybe then on the other diagonal ?
		else if (isBlockingDecreasingAxis()) {
			blockingDecreasingAxis();
		}

	}

	static int countUpNDownCross() {

		int up, down, left, right;

		up = boardSize - queenRow - upBlock;
		down = queenRow - 1 - downBlock;
		left = queenColumn - 1 - leftBlock;
		right = boardSize - queenColumn - rightBlock;

		return up + down + left + right;

	}

	static int countDiagonalCross() {
		int leftUp, leftDown, rightUp, rightDown;
		leftUp = boardSize - Math.max(queenRow, boardSize + 1 - queenColumn) - leftUpBlock;
		leftDown = boardSize - Math.max(boardSize + 1 - queenRow, boardSize + 1 - queenColumn) - leftDownBlock;
		rightUp = boardSize - Math.max(queenRow, queenColumn) - rightUpBlock;
		rightDown = boardSize - Math.max(boardSize + 1 - queenRow, queenColumn) - rightDownBlock;

		return leftUp + leftDown + rightUp + rightDown;

	}

	static int queensAttack(int n, int k, int r_q, int c_q, int[][] obstacles) {
		boardSize = n;
		queenRow = r_q;
		queenColumn = c_q;

		for (int i = 0; i < k; i++) {
			currentRow = obstacles[i][0];
			currentColumn = obstacles[i][1];
			checkCurrentObstacle();

		}

		return countUpNDownCross() + countDiagonalCross();
	}

	public static void main(String[] args) throws IOException {
		Scanner scanner = new Scanner(System.in);

        String[] nk = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        String[] r_qC_q = scanner.nextLine().split(" ");

        int r_q = Integer.parseInt(r_qC_q[0]);

        int c_q = Integer.parseInt(r_qC_q[1]);

        int[][] obstacles = new int[k][2];

        for (int i = 0; i < k; i++) {
            String[] obstaclesRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 2; j++) {
                int obstaclesItem = Integer.parseInt(obstaclesRowItems[j]);
                obstacles[i][j] = obstaclesItem;
            }
        }

        int result = queensAttack(n, k, r_q, c_q, obstacles);

        System.out.println((String.valueOf(result)));
        

        scanner.close();
    }
}
