import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
/*
This is a solution for problem :
https://www.hackerrank.com/challenges/non-divisible-subset/problem


*/
class Result {

  
 
    public static int longestLength =0 ;
    public static int [] arrayOfRemainders;

    public static int nonDivisibleSubset(int k, List<Integer> s) {
    int arrLength = k;
    arrayOfRemainders = new int[k];
    List<Integer> temp = new ArrayList<Integer>();
    
    for(int n : s) {
          arrayOfRemainders[n%k]++;
    }
    for(int i =1; i <= arrLength/2 ; i++){
        if(i*2 == k){
            if(arrayOfRemainders[i]>0)longestLength++;
            continue;
        }
         longestLength+= Integer.compare(arrayOfRemainders[i], arrayOfRemainders[arrLength-i]) >= 0 ?               arrayOfRemainders[i] :
         arrayOfRemainders[arrLength-i] ;

    }
            if(arrayOfRemainders[0]>0)longestLength++;
            return longestLength;
    }

    

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(firstMultipleInput[0]);

        int k = Integer.parseInt(firstMultipleInput[1]);

        List<Integer> s = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Integer::parseInt)
            .collect(toList());

        int result = Result.nonDivisibleSubset(k, s);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}
